from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Technician, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder





@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician not exist"})
            response.status_code = 404
            return response
    else: # PUT
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)

            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        appointments_with_vip = []

        automobiles = AutomobileVO.objects.all()

        for appointment in appointments:
            try:
                # Try to find an AutomobileVO with matching vin
                automobile_vo = automobiles.get(vin=appointment.vin)
                if automobile_vo.sold:
                    appointment.vip = True
                else:
                    appointment.vip = False
            except AutomobileVO.DoesNotExist:
                # If no matching AutomobileVO found, set vip attribute to False
                appointment.vip = False

            # Add the appointment to the list with vip attribute
            appointments_with_vip.append(appointment)

        return JsonResponse(
            {"appointments": appointments_with_vip},
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(pk=technician_id)

            # Update the content dictionary with technician and status values
            content["technician"] = technician
            content["status"] = "created"

            # Create a new Appointment object using the updated content dictionary
            appointment = Appointment(**content)
            appointment.save()
            appointments = Appointment.objects.all()

            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except KeyError:
            response = JsonResponse(
                {"message": "Invalid JSON payload or missing fields"},
                status=400
            )
            return response
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician ID does not exist"},
                status=404
            )
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "canceled"
            appointment.save()
            return JsonResponse(
                {"message": "Appointment canceled successfully"},
                status=200
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            content = json.loads(request.body)

            if "status" in content:
                status = content["status"].lower()
                if status == "canceled":
                    appointment.status = "canceled"
                elif status == "finished":
                    appointment.status = "finished"
                else:
                    response = JsonResponse({"message": "Invalid status value"})
                    response.status_code = 400
                    return response

            appointment.save()
            return JsonResponse(
                {"message": "Appointment updated successfully"},
                status=200
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_appointment_canceled(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "canceled"  
        appointment.save()  
        return JsonResponse(
            {"message": "Appointment canceled successfully"},
            status=200
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_appointment_finished(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "finished"  
        appointment.save() 
        return JsonResponse(
            {"message": "Appointment finished successfully"},
            status=200
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response