from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, blank=False)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField(unique=True)
       
    
    def __str__(self):
        return f"{self.last_name} {self.first_name} - {self.employee_id}"
    
    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})
    
class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=17, blank=False)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)
    
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
    
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="appointments",
        null=True,
        on_delete=models.CASCADE
    )
    
    def __str__(self):
        return self.customer
    
    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
    