import React, { useEffect, useState } from "react";

function VehicleModelForm () {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.picture_url = picture;
        data.manufacturer_id = manufacturer;

        console.log(data);

        const vehicleModelsUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(vehicleModelsUrl, fetchConfig);
        if (response.ok) {
            const newVehicleModel = await response.json();
            console.log(newVehicleModel);

            setName('');
            setPicture('');
            setManufacturer('');

        }
    }

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }



    const [manufacturers, setManufacturers] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers)
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    return (

        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" value={name}/>
                        <label htmlFor="model_name">Model name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureChange} placeholder="Picture URL" required type="url" name="picture" id="picture" className="form-control" value={picture}/>
                        <label htmlFor="picture">Picture URL...</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" className="form-select" value={manufacturer}>
                        <option value="">Choose a Manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    );

};

export default VehicleModelForm;
