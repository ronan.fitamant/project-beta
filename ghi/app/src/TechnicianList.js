import React, { useState, useEffect } from "react";

function TechnicianList(props) {

  const [technicians, setTechnicians] = useState([])

  async function fetchTechnicians() {
    const response = await fetch('http://localhost:8080/api/technicians/');
  
    if (response.ok) {
      const data = await response.json()
      console.log(data)
      setTechnicians(data.technicians)
    }
}


  useEffect(() => {
    fetchTechnicians();
  }, []);

  const handleDelete = async (id) => {
    const deleteUrl = `http://localhost:8080/api/technicians/${id}`
    const fetchConfig = {
      method: "delete",
    }

    const response = await fetch(deleteUrl, fetchConfig)
    if (response.ok) {
      fetchTechnicians();
    } else {
      console.error(response)
    }
  }

    return(
      <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map(technician => {
          return (
            <tr key={technician.href}>
              <td>{ technician.first_name }</td>
              <td>{ technician.last_name }</td>
              <td>{ technician.employee_id }</td>
              <td>
                <button onClick={() => handleDelete(technician.id)} className="btn btn-danger">Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    );
  }

  export default TechnicianList;
