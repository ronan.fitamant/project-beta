import React, { useState, useEffect } from "react";

function AppointmentList(props) {
  const [appointments, setAppointments] = useState([]);
  const [autos, setAutos] = useState([]);

  async function fetchAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  async function fetchAutos() {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }

  useEffect(() => {
    fetchAppointments();
  }, []);

  useEffect(() => {
    fetchAutos();
  }, []);

  const handleCancel = async (id) => {
    const confirmation = window.confirm(
      "Are you sure you want to cancel this appointment?"
    );
    if (confirmation) {
      const cancelUrl = `http://localhost:8080/api/appointments/${id}/canceled/`;
      const fetchConfig = {
        method: "PUT",
      };

      const response = await fetch(cancelUrl, fetchConfig);
      if (response.ok) {
        // Update appointments state by removing the canceled appointment
        setAppointments((prevAppointments) =>
          prevAppointments.filter((appointment) => appointment.id !== id)
        );
      }
    }
  };

  const handleFinish = async (id) => {
    const confirmation = window.confirm("Are you sure everything is finished?");
    if (confirmation) {
      const finishUrl = `http://localhost:8080/api/appointments/${id}/finished/`;
      const fetchConfig = {
        method: "PUT",
      };

      const response = await fetch(finishUrl, fetchConfig);
      if (response.ok) {
        // Update appointments state by removing the finished appointment
        setAppointments((prevAppointments) =>
          prevAppointments.filter((appointment) => appointment.id !== id)
        );
      }
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Vin</th>
          <th>VIP?</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
          const auto = autos.find((auto) => auto.vin === appointment.vin);
          const isVIP = auto && auto.sold; // Check if appointment is a "VIP"
          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{isVIP ? "Yes" : "No"}</td>
              <td>{appointment.customer}</td>
              <td>
                {new Date(appointment.date_time).toLocaleDateString()}
              </td>
              <td>
                {new Date(appointment.date_time).toLocaleTimeString([], {
                  hour: "2-digit",
                  minute: "2-digit",
                })}
              </td>
              <td>
                {appointment.technician.last_name}{" "}
                {appointment.technician.first_name}
              </td>
              <td>{appointment.reason}</td>
              <td>
                <button
                  onClick={() => handleCancel(appointment.id)}
                  className="btn btn-warning"
                >
                  Cancel
                </button>
              </td>
              <td>
                <button
                  onClick={() => handleFinish(appointment.id)}
                  className="btn btn-success"
                >
                  Finish
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;
