import { useState, useEffect } from "react";

function SalespeopleList(props) {
    const [salespersons, setSalespeople] = useState([]);

    async function fetchSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons);
        }
    }


    useEffect(() => {
        fetchSalespeople();
    }, [])


    return (
        <div className="customer-list">
        <h2 className="customer-list-title">Salespeople List</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespersons.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )
};

export default SalespeopleList;
