import { useState, useEffect } from "react";

function SaleList(props) {
  const [sales, setSales] = useState([]);

  async function fetchSales() {
    const response = await fetch('http://localhost:8090/api/sales/');

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  }

  useEffect(() => {
    fetchSales();
  }, [])

  return (
    <div className="customer-list">
      <h2 className="customer-list-title">Sales</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price.toLocaleString("en-US", { style: "currency", currency: "USD" })}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
};

export default SaleList;
