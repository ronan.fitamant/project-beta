function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <img
            src="https://vlkarchitects.com/assets/projects/_1200x630_crop_center-center_82_none/Automotive-Architects-Luxury-Dealership-Design.jpg?mtime=1506445195"
            alt="Car dealership"
            style={{ maxWidth: '150%', height: 'auto' }}
          />
        </div>
      </div>
    </div>
  );
}

export default MainPage;
