import React, { useState, useEffect } from 'react';

function AppointmentForm() {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [reason, setReason] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [sales, setSales] = useState([]);

  const handleAutomobileVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleDateChange = (event) => {
    const value = event.target.value;
    setDate(value);
  };

  const handleTimeChange = (event) => {
    const value = event.target.value;
    setTime(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const fetchTechnicians = async () => {
    const response = await fetch('http://localhost:8080/api/technicians/');

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const fetchSales = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };

  useEffect(() => {
    fetchTechnicians();
    fetchSales();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      vin,
      customer,
      date_time: date + ' ' + time,
      reason,
      technician
    };
    console.log(data);

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);

      setVin('');
      setCustomer('');
      setDate('');
      setTime('');
      setReason('');
      setTechnician('');
    }
  };

  // Check if the entered VIN exists in the sold cars list
  const isVinSold = sales.some(sale => sale.automobile.vin === vin);
  const isVIP = isVinSold ? 'VIP' : '';

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-a-service-appointments">
            <div className="form-floating mb-3">
              <input onChange={handleAutomobileVinChange} value={vin} placeholder="Automobile VIN" id="vin" required name="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required name="customer" type="text" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleDateChange} value={date} placeholder="mm/dd/yyyy" type="date" name="date" id="date" className="form-control"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTimeChange} value={time} placeholder="hh:mm" type="time" name="time" id="time" className="form-control" />
              <label htmlFor="time">Time</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleReasonChange} value={reason} placeholder="Enter reason..." type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="mb-3">
              <select onChange={handleTechnicianChange} value={technician} required name="technician" className="form-select" id="technician">
                <option value="">Choose a Technician</option>
                {technicians.map((technician) => (
                  <option key={technician.id} value={technician.id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
      {isVinSold && (
        <div className="offset-3 col-6 mt-3">
          <div className="alert alert-warning" role="alert">
            VIN {vin} is associated with a sale. Customer is {isVIP}.
          </div>
        </div>
      )}
    </div>
  );
}

export default AppointmentForm;
