import React, { useState, useEffect } from "react";

function ServiceList(props) {
  const [filteredAppointments, setFilteredAppointments] = useState([]);
  const [searchVin, setSearchVin] = useState("");
  const [appointments, setAppointments] = useState([]);
  const [autos, setAutos] = useState([]);

  async function fetchAutos() {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }

  async function fetchAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");

    if (response.ok) {
      const data = await response.json();
      const appointmentsData = data.appointments;

      // Update VIP status based on sold status from fetched autos
      const updatedAppointments = appointmentsData.map((appointment) => {
        const auto = autos.find((auto) => auto.vin === appointment.vin);
        return {
          ...appointment,
          vip: auto?.sold ? "Yes" : "No",
        };
      });

      setAppointments(updatedAppointments);
      setFilteredAppointments(updatedAppointments);
    }
  }

  useEffect(() => {
    fetchAutos();
  }, []);

  useEffect(() => {
    fetchAppointments();
  }, [autos]);

  const handleSearch = (event) => {
    event.preventDefault();
    const filtered = appointments.filter(
      (appointment) => appointment.vin === searchVin
    );
    setFilteredAppointments(filtered);
  };

  return (
    <div>
      <form onSubmit={handleSearch}>
        <label>
          Search by VIN:
          <input
            type="text"
            value={searchVin}
            onChange={(event) => setSearchVin(event.target.value)}
          />
        </label>
        <button type="submit">Search</button>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {/* Check if there are filtered appointments */}
          {filteredAppointments.length > 0 ? (
            filteredAppointments.map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip}</td>
                <td>{appointment.customer}</td>
                <td>
                  {new Date(appointment.date_time).toLocaleDateString()}
                </td>
                <td>
                  {new Date(appointment.date_time).toLocaleTimeString([], {
                    hour: "2-digit",
                    minute: "2-digit",
                  })}
                </td>
                <td>
                  {appointment.technician.last_name}
                  {appointment.technician.first_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))
          ) : (
            appointments.map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip}</td>
                <td>{appointment.customer}</td>
                <td>
                  {new Date(appointment.date_time).toLocaleDateString()}
                </td>
                <td>
                  {new Date(appointment.date_time).toLocaleTimeString([], {
                    hour: "2-digit",
                    minute: "2-digit",
                  })}
                </td>
                <td>
                  {appointment.technician.last_name}
                  {appointment.technician.first_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceList;
