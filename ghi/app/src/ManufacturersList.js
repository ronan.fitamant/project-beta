import { useState, useEffect } from "react";

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([]);

    async function fetchManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }


    useEffect(() => {
        fetchManufacturers();
    }, [])


    return (
        <div className="customer-list">
        <h2 className="customer-list-title">Manufacturer</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{ manufacturer.name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )
};

export default ManufacturerList;
