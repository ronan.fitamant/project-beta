import React, { useState, useEffect } from "react";

const SalesNewForm = () => {
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      automobile: vin,
      salesperson: salespersonName,
      customer: customerName,
      price: price,
    };

    console.log(data);

    const salesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesUrl, fetchConfig);

    if (response.ok) {
      const newSale = await response.json();
      console.log(newSale);

      // Update the "sold" property of the automobile
      const updateUrl = `http://localhost:8100/api/automobiles/${vin}/`;
      const updateConfig = {
        method: "put",
        body: JSON.stringify({ sold: true }),
        headers: {
          "Content-Type": "application/json",
        },
      };


      const updateResponse = await fetch(updateUrl, updateConfig);

      if (updateResponse.ok) {
        console.log("Automobile updated successfully");
      } else {
        console.log("Failed to update the automobile");
      }

      setAutomobile("");
      setSalesperson("");
      setCustomer("");
      setPrice("");
    }
  };

  const [vin, setAutomobile] = useState("");
  const handleVinChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  };

  const [salespersonName, setSalesperson] = useState("");
  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const [customerName, setCustomer] = useState("");
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const [price, setPrice] = useState("");
  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const [autos, setAutomobiles] = useState([]);
  const fetchData2 = async () => {
    const url = "http://localhost:8100/api/automobiles/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    fetchData2();
  }, []);

  const [salespersons, setSalespersons] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8090/api/salespeople/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salespersons);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const [customers, setCustomers] = useState([]);
  const fetchData3 = async () => {
    const url = "http://localhost:8090/api/customers/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchData3();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new Sale</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="mb-3">
              <select onChange={handleVinChange} required id="vin" name="vin" className="form-select" value={vin}>
                <option value="">Choose an automobile VIN...</option>
                {autos.map((automobile) => (
                  <option key={automobile.vin} value={automobile.vin}>
                    {automobile.vin}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select" value={salespersonName}>
                <option value="">Choose a salesperson...</option>
                {salespersons.map((salesperson) => (
                  <option key={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleCustomerChange} required id="customer" name="customer" className="form-select" value={customerName}>
                <option value="">Choose a customer...</option>
                {customers.map((customer) => (
                  <option key={customer.id} value={customer.id}>
                    {`${customer.last_name}, ${customer.first_name}`}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control" value={price} />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SalesNewForm;
