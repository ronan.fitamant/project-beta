import React, {useState, useEffect} from 'react';

function AutomobileForm () {

    const [vin, setVin] = useState('')

    const handleVinChange =(event) => {
        const value = event.target.value
        setVin(value)
    }

    const [color, setColor] = useState('')

    const handleColorChange =(event) => {
        const value = event.target.value
        setColor(value)
    }

    const [year, setYear] = useState('')

    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
      }

    const [model, setModel] = useState('')

    const handleModelChange =(event) => {
        const value = event.target.value
        setModel(value)
    }

    const [models, setModels] = useState([])


    const getData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setModels(data.models);
        }
    }

    useEffect(()=> {
        getData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.vin = vin;
        data.color = color
        data.year = year
        data.model_id = model

        console.log(data);

        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

    const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
          const newAutomobile= await response.json();
          console.log(newAutomobile);

          setVin('');
          setColor('');
          setYear('');
          setModel('');

        }
      }


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an Automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-a-automobile">
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} value={vin} placeholder="VIN" id="vin" type= "text" required name="vin" className="form-control" />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="Color" required name="color" type="text" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleYearChange} value={year} placeholder="Year" type="number" name="year" id="year" className="form-control" />
              <label htmlFor="year">Year</label>
            </div>
            <div className="mb-3">
              <select onChange={handleModelChange} value={model} required name="model" className="form-select" id="model">
                <option value="">Choose a model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>
                        {model.name}
                        </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
