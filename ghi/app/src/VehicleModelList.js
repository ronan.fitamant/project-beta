import { useState, useEffect } from "react";

function VehicleModelList(props) {
    const [models, setModels] = useState([]);

    async function fetchModels() {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }


    useEffect(() => {
        fetchModels();
    }, [])


    return (
        <div className="customer-list">
        <h2 className="customer-list-title">Models</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer Name</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{ model.name }</td>
                            <td>{ model.manufacturer.name }</td>
                            <td><img src={model.picture_url} alt={`${model.fabric} ${model.style_name} ${model.color}`} className="img-thumbnail" style={{ maxWidth: '10em', maxHeight: '10em' }}/></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )
};

export default VehicleModelList;
