import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceList from './ServiceList';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturersList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';
import SalesNewForm from './SaleNewForm';
import SaleList from './SaleList';
import SalespersonHistoryList from './SalespersonHistoryList';
import AutomobileList from './AutomobileList';
import AutomobileForm from './CreateAnAutomobileForm';

function App(props) {
  return (
    <>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="technicians">
              <Route index element={<TechnicianList technicians={props.technicians} />} />
              <Route path="create" element={<TechnicianForm />} />
            </Route>
            <Route path="appointments">
              <Route index element={<AppointmentList />} />
              <Route path="create" element={<AppointmentForm />} />
              <Route path="history" element={<ServiceList />} />
            </Route>
            <Route path='salespeople'>
              <Route index element={<SalespeopleList salespeople={props.salespeople} />} />
              <Route path='new' element={<SalespersonForm />} />
            </Route>
            <Route path='customers'>
              <Route index element={<CustomerList customers={props.customers} />} />
              <Route path='new' element={<CustomerForm />} />
            </Route>
            <Route path='manufacturers'>
              <Route index element={<ManufacturerList manufacturers={props.manufacturers} />} />
              <Route path='new' element={<ManufacturerForm />} />
            </Route>
            <Route path='models'>
              <Route index element={<VehicleModelList models={props.models} />} />
              <Route path='new' element={<VehicleModelForm />} />
            </Route>
            <Route path='sales'>
              <Route index element={<SaleList sales={props.sales} />} />
              <Route path='new' element={<SalesNewForm />} />
              <Route path='history' element={<SalespersonHistoryList />} />
            </Route>
            <Route path='automobiles'>
              <Route index element={<AutomobileList models={props.automobiles} />} />
              <Route path='create' element={<AutomobileForm />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
