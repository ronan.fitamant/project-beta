import React, { useState } from "react";

function ManufacturerForm () {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;


        console.log(data);

        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturersUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer);

            setName('');
        }
    }

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }




    return (

        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Manufacturer Name" required type="text" name="manufacturer_name" id="manufacturer_name" className="form-control" value={name}/>
                        <label htmlFor="manufacturer_name">Manufacturer name...</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>

    );

};

export default ManufacturerForm;
