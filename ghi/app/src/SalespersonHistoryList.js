import React, { useState, useEffect } from "react";

function SalespersonHistoryList(props) {
  const [sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");

  async function fetchSales() {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  }

  useEffect(() => {
    fetchSales();
  }, []);

  useEffect(() => {
    if (selectedSalesperson) {
      const filtered = sales.filter(
        (sale) => sale.salesperson.id === parseInt(selectedSalesperson)
      );
      setFilteredSales(filtered);
    } else {
      setFilteredSales([]);
    }
  }, [selectedSalesperson, sales]);

  const uniqueSalespersons = Array.from(
    new Set(sales.map((sale) => sale.salesperson.id))
  );

  const salespersonOptions = uniqueSalespersons.map((salespersonId) => {
    const sale = sales.find((sale) => sale.salesperson.id === salespersonId);
    return {
      value: sale.salesperson.id,
      label: `${sale.salesperson.first_name} ${sale.salesperson.last_name}`,
    };
  });

  if (!selectedSalesperson) {
    return (
      <div>
        <h2>Salesperson History</h2>
        <label htmlFor="salesperson-select"></label>
        <select
          id="salesperson-select"
          value={selectedSalesperson}
          onChange={(event) => setSelectedSalesperson(event.target.value)}
        >
          <option value="">Select a salesperson...</option>
          {salespersonOptions.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </div>
    );
  }

  return (
    <div>
      <h2>Salesperson History</h2>
      <label htmlFor="salesperson-select"></label>
      <select
        id="salesperson-select"
        value={selectedSalesperson}
        onChange={(event) => setSelectedSalesperson(event.target.value)}
      >
        <option value="">All Salespersons</option>
        {salespersonOptions.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {filteredSales.map((sale) => (
            <tr key={sale.id}>
              <td>
                {sale.salesperson.first_name} {sale.salesperson.last_name}
              </td>
              <td>
                {sale.customer.first_name} {sale.customer.last_name}
              </td>
              <td>{sale.automobile.vin}</td>
              <td>${sale.price.toLocaleString("en-US", { minimumFractionDigits: 2 })}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalespersonHistoryList;
