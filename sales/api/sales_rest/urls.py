from django.urls import path

from .views import (
        api_salesperson,
        api_salesperson_detail,
        api_customers, api_customer,
        api_sales,
        api_sale
    )

urlpatterns = [
    path("salespeople/", api_salesperson, name="api_salesperson"),
    path("salespeople/<int:pk>/", api_salesperson_detail, name="api_salesperson_detail"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:pk>/", api_sale, name="api_sale"),
]
